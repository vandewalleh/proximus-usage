#!/usr/bin/python3

import requests
import yaml


def format_bytes(num):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti']:
        if abs(num) < 1024.0:
            return "%3.1f%sB" % (num, unit)
        num /= 1024.0
    return num


def main():
    with open('info.yaml', 'r') as f:
        info = yaml.full_load(f)

        user_name = info['login']['username']
        password = info['login']['password']

        products = {}
        for product in info['products']:
            name = product['name']
            id = product['id']
            products[name] = id

    session = requests.Session()
    login(session, user_name, password)

    for name, id in products.items():
        print(usage(id, name, session))


def login(session, user_name, password):
    auth_url = 'https://www.proximus.be/auth/json/authenticate'

    r = session.post(auth_url)
    print('Authentication part 1', r.status_code)

    if r.status_code != requests.codes.ok:
        exit(1)

    json = r.json()
    json['callbacks'][0]['input'][0]['value'] = user_name
    json['callbacks'][1]['input'][0]['value'] = password

    r = session.post(auth_url, json=json)

    if r.status_code != requests.codes.ok:
        exit(1)

    print('Authentication part 2', r.status_code)


def usage(id, name, session):
    url = f'https://www.proximus.be/rest/usage-product-internet/usage/service/{id}/details/data'
    r = session.get(url)

    if r.status_code != requests.codes.ok:
        print(r.status_code)
        exit(1)

    json = r.json()
    data = json['reportingCategoryGroups'][0]['reportingCategories'][0]
    usage_bytes = data['usage']
    max_bytes = data['allowance']['threshold']

    usage_human = format_bytes(usage_bytes)
    max_human = format_bytes(max_bytes)

    return f'{name}:\t {usage_human}/{max_human}'


if __name__ == '__main__':
    main()
